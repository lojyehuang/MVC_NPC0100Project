﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NPC0100Project.Models
{
    public class Project
    {
        [Display(Name = "編號")]
        public int ID { get; set; }

        // user ID from AspNetUser table.
        public string OwnerID { get; set; }

        [Required(ErrorMessage = "專案名稱不可以為空白")]
        [StringLength(200)]
        [Display(Name = "專案名稱")]
        public string ProjectName { get; set; }

        [Display(Name = "專案描述")]
        public string Description { get; set; }


        [Required]
        [Display(Name = "專案成員")]
        public string Members { get; set; }


        public ContactStatus Status { get; set; }
    }

    public enum ContactStatus
    {
        Submitted,
        Approved,
        Rejected
    }
}
