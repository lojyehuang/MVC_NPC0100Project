﻿using Microsoft.EntityFrameworkCore;
using NPC0100Project.Data;
using NPC0100Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NPC0100Project.Repository
{


    public class ProjectRepository : IRepository<Project, int>
    {
        private readonly ProjectContext _context;
        public ProjectRepository(ProjectContext context) => this._context = context;

        

        public async Task<IEnumerable<Project>> GetAll()
        {
            return await _context.Projects.ToListAsync();
        }

        public IEnumerable<Project> GetAllQueryable()
        {
            return _context.Projects.AsQueryable();
        }

        public async Task<Project> GetById(int id)
        {
            return await _context.Projects.FindAsync(id);
        }

        public async Task<Project> Insert(Project entity)
        {
            await _context.Projects.AddAsync(entity);
            return entity;
        }

        public async Task Delete(int id)
        {
            var project = await _context.Projects.FirstOrDefaultAsync(p => p.ID == id);
            if (project != null)
            {
                _context.Remove(project);
            }
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public Project Update(Project entity)
        {
            //await _context.Update(entity);
            _context.Set<Project>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            return entity;
        }


    }
}
