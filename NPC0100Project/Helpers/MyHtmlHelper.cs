﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NPC0100Project.Helpers
{
    public static class MyHtmlHelper
    {
        public static IHtmlContent DisplayWithBreaksFor<TModel, TResult>(this IHtmlHelper<TModel> html, Expression<Func<TModel, TResult>> expression)
        {
            ModelExpressionProvider modelExpressionProvider = (ModelExpressionProvider)html.ViewContext.HttpContext.RequestServices.GetService(typeof(IModelExpressionProvider));
            var modelExplorer = modelExpressionProvider.CreateModelExpression(html.ViewData, expression);
            //var modelExplorer = ModelExpressionProvider.CreateModelExpression(expression, html.ViewData, html.MetadataProvider);
            var model = html.Encode(modelExplorer.Model.ToString().Replace("\r\n", "XXXYYYZZZ")).Replace("XXXYYYZZZ", "<br />");
            if (String.IsNullOrEmpty(model))
                return HtmlString.Empty;

            return html.Raw(model);

        }
    }
}
