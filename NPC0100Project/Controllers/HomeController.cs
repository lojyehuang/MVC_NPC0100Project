﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NPC0100Project.Models;

namespace NPC0100Project.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;

        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }
        [Authorize]
        public IActionResult Index()
        {
            //可以使用 HttpContext.User.Identity.IsAuthenticated 來判斷用戶是否登入
            //但由於此Action已經加上[Authorize]，表示會執行此Action內容的一定都是登入者，所以不必再脫褲子放屁多寫判斷XD

            //StringBuilder sb = new StringBuilder();
            //StringBuilder ua = new StringBuilder();
            //sb.AppendLine("<ul>");

            //foreach (Claim claim in HttpContext.User.Claims)
            //{
            //    sb.AppendLine($@"<li> claim.Type:{claim.Type} , claim.Value:{ claim.Value}</li>");
            //}
            //foreach (Claim claim in HttpContext.User.Claims)
            //{
            //    ua.AppendLine(claim.Value);
            //}
            //sb.AppendLine("</ul>");
            //ViewBag.msg = sb.ToString();
            //ViewBag.useraccount = ua.ToString();
            return View(nameof(Index));
        }

        /// <summary>
        /// 登入頁
        /// </summary>
        /// <returns></returns>
        //public IActionResult Login()
        //{
        //    return View();
        //}
        /// <summary>
        /// 表單post提交，準備登入
        /// </summary>
        //// <param name="form"></param>
        /// <returns></returns>
        //[HttpPost]
        //public async Task<IActionResult> Login(string Account, string pd, string ReturnUrl)
        //{
        //    //未登入者想進入必須登入的頁面，他會被自動導頁至/Home/Login，網址後面也會自動帶上名為ReturnUrl(原始要求網址)的QueryString

        //    //pd是密碼，記得加密pd變數或雜湊過後再和DB資料比對
        //    //從自己DB檢查帳&密，輸入是否正確
        //    if ((Account == "shadow" && pd == "shadow") == false)
        //    {
        //        //帳&密不正確
        //        ViewBag.errMsg = "帳號或密碼輸入錯誤";
        //        return View();//流程不往下執行
        //    }
        //    //帳密都輸入正確，ASP.net Core要多寫三行程式碼 
        //    //Claim[] claims = new[] { new Claim("Account", Account) }; //Key取名"Account"，在登入後的頁面，讀取登入者的帳號會用得到，自己先記在大腦
        //    //改使用ClaimTypes.Name就不必寫死字串， Account變數為用戶輸入的帳號
        //    Claim[] claims = new[] { new Claim(ClaimTypes.Name, Account) };
            
        //    ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);//Scheme必填
        //    ClaimsPrincipal principal = new ClaimsPrincipal(claimsIdentity);
        //    //從組態讀取登入逾時設定
        //    double loginExpireMinute = _config.GetValue<double>("LoginExpireMinute");
        //    //執行登入，相當於以前的FormsAuthentication.SetAuthCookie()
        //    await HttpContext.SignInAsync(principal,
        //        new AuthenticationProperties()
        //        {
        //            IsPersistent = false, //IsPersistent = false：瀏覽器關閉立馬登出；IsPersistent = true 就變成常見的Remember Me功能
        //                                  //用戶頁面停留太久，逾期時間，在此設定的話會覆蓋Startup.cs裡的逾期設定
        //            /* ExpiresUtc = DateTime.UtcNow.AddMinutes(loginExpireMinute) */
        //        });
        //    //加上 Url.IsLocalUrl 防止Open Redirect漏洞
        //    if (!string.IsNullOrEmpty(ReturnUrl) && Url.IsLocalUrl(ReturnUrl))
        //    {
        //        return Redirect(ReturnUrl);//導到原始要求網址
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "Home");//到登入後的第一頁，自行決定
        //    }
        //}

        ///// <summary>
        ///// 登出
        ///// </summary>
        ///// <returns></returns>
        ////登出 Action 記得別加上[Authorize]，不管用戶是否登入，都可以執行Logout
        //public async Task<IActionResult> Logout()
        //{
        //    await HttpContext.SignOutAsync();

        //    return RedirectToAction("Login", "Home");//導至登入頁
        //}

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
