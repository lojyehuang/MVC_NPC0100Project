﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NPC0100Project.Areas.Identity.Data;
using NPC0100Project.Authorization;
using NPC0100Project.Data;
using NPC0100Project.Models;
using NPC0100Project.Repository;


namespace NPC0100Project.Controllers
{
    public class ProjectController : Controller
    {
        //private readonly ProjectContext _context;
        private readonly IRepository<Project, int> _projectRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAuthorizationService _authorizationService;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public ProjectController(IRepository<Project,int> projectRepository, UserManager<ApplicationUser> userManager, 
            IAuthorizationService authorizationService, IWebHostEnvironment hostingEnvironment)
        {
            _projectRepository = projectRepository;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _hostingEnvironment = hostingEnvironment;
        }

        protected AuthDbContext Context { get; }
        //protected IAuthorizationService AuthorizationService { get; }
        //protected UserManager<ApplicationUser> UserManager { get; }

        // GET: Project
        //public async Task<IActionResult> Index()
        //{
        //    //return View(await _context.Projects.ToListAsync());
        //    //IProjectRepository objProjectRepository = new ProjectRepository(_context);
        //    var projects = await _projectRepository.GetAll();
        //    return View(projects);
        //}



        //[HttpPost]
        [Authorize]
        public IActionResult Index(string sortOrder)
        {
            ViewBag.MemberSort = sortOrder == "Members" ? "Member_desc" : "Members";
            var query = _projectRepository.GetAllQueryable();
            switch (sortOrder)
            {
                case "Members":
                    query = query.OrderBy(p => p.Members);
                    break;
                case "Member_desc":
                    query = query.OrderByDescending(p => p.Members);
                    break;
                default:
                    break;
            }
            return View(query.ToList());
        }

        //GET: Project/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Project/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Project project)
        {
            if (ModelState.IsValid)
            {

                //var user = await _userManager.FindByIdAsync(userId);
                //if (user == null)
                //{
                //    ViewBag.ErrorMessage = $"User with Id = {userId} cannot be found";
                //    return View("NotFound");
                //}
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
                project.OwnerID = userId;
                //project.OwnerID = userId;
                // requires using ContactManager.Authorization;
                var isAuthorized = await _authorizationService.AuthorizeAsync(
                                                            User, project,
                                                            ContactOperations.Create);
                if (!isAuthorized.Succeeded)
                {
                    return Forbid();
                }


                await _projectRepository.Insert(project);
                await _projectRepository.Save();                
                return RedirectToAction(nameof(Index));
            }
            return View(project);
        }




        // GET: Project/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var project = _projectRepository.GetById(id);
            var project = await _projectRepository.GetById((int)id);

            if (project == null)
            {
                return NotFound();
            }

            var isAuthorized = await _authorizationService.AuthorizeAsync(
                                                     User, project,
                                                     ContactOperations.Delete);
            if (!isAuthorized.Succeeded)
            {
                return Forbid();
            }


            return View(project);
        }

        // POST: Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var project = await _projectRepository.GetById((int)id);

            var isAuthorized = await _authorizationService.AuthorizeAsync(
                                                     User, project,
                                                     ContactOperations.Delete);
            if (!isAuthorized.Succeeded)
            {
                return Forbid();
            }

            await _projectRepository.Delete(id);
            await _projectRepository.Save();
            
            return RedirectToAction(nameof(Index));
        }

        // GET: Project/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var project = await _projectRepository.GetById((int)id);
            if (project == null)
            {
                return NotFound();
            }

            return View(project);
        }



        // GET: Project/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var project = await _projectRepository.GetById((int)id);
            if (project == null)
            {
                return NotFound();
            }
            return View(project);
        }

        // POST: Project/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,ProjectName,Description,Members")] Project project)
        {
            if (id != project.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _projectRepository.Update(project);
                    await _projectRepository.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectExists(project.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(project);
        }


        public async Task<IActionResult> Excel()
        {
            using (var workbook = new XLWorkbook())
            {

                var projects = await _projectRepository.GetAll();

                var worksheet = workbook.Worksheets.Add("Projects");
                var currentRow = 2;
                var title = worksheet.Range("B1");
                var header = worksheet.Range("A2:C2");
                var Allcontent = worksheet.Range("A2:C"+ (projects.Count()+2).ToString());
                

                title.Value = "我們的專案";
                title.Style.Font.FontName = "標楷體";
                title.Style.Font.FontSize = 30;
                title.Style.Fill.BackgroundColor = XLColor.FromArgb(173, 223, 247);
                title.Style.Font.FontColor = XLColor.FromArgb(58, 92, 228);
                title.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                title.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                header.Style.Fill.BackgroundColor = XLColor.FromArgb(186,253,215);
                header.Style.Font.FontColor = XLColor.Black;
                header.Style.Font.Bold = true;
                header.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                worksheet.Column("B").Style.Alignment.WrapText = false;

                worksheet.Column("A").Width = 15;
                worksheet.Column("B").Width = 80;
                worksheet.Column("C").Width = 30;
                worksheet.Row(1).Height = 42;

                Allcontent.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                Allcontent.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                worksheet.Cell(currentRow, 1).Value = "Project Name";
                worksheet.Cell(currentRow, 2).Value = "Description";
                worksheet.Cell(currentRow, 3).Value = "Members";
                worksheet.RowHeight = 15;


                

                foreach (var pj in projects)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = pj.ProjectName;
                    worksheet.Cell(currentRow, 2).Value = pj.Description;
                    worksheet.Cell(currentRow, 3).Value = pj.Members;
                }


                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    return File(content, 
                        "appplication/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "ProjectInfo_"+DateTime.Now.ToString("yyyy年MM月dd日_hh時mm分ss秒")+".xlsx");
                }
            }
        }

        public IActionResult RunBot()
        {
            ProcessStartInfo psi =

            new ProcessStartInfo(_hostingEnvironment.WebRootPath+@"\啟動檔.BAT");

            psi.RedirectStandardError = true;
            psi.RedirectStandardOutput = true;

            psi.WindowStyle = ProcessWindowStyle.Maximized;

            psi.UseShellExecute = false;

            Process listFiles;

            listFiles = Process.Start(psi);

            StreamReader myOutput = listFiles.StandardOutput;

            listFiles.WaitForExit(2000);

            string output = myOutput.ReadToEnd();
            string error = myOutput.ReadToEnd();
            int exitCode;
            exitCode = listFiles.ExitCode;
            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            listFiles.Close();

            ViewBag.output = output;
            ViewBag.error = error;

            //ProcessStartInfo processInfo;
            //Process process;

            //processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            //processInfo.CreateNoWindow = true;
            //processInfo.UseShellExecute = false;
            //// *** Redirect the output ***
            //processInfo.RedirectStandardError = true;
            //processInfo.RedirectStandardOutput = true;

            //process = Process.Start(processInfo);
            //process.WaitForExit();

            //// *** Read the streams ***
            //// Warning: This approach can lead to deadlocks, see Edit #2
            //string output = process.StandardOutput.ReadToEnd();
            //string error = process.StandardError.ReadToEnd();

            //exitCode = process.ExitCode;

            //Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            //Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            //Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            //process.Close();

            //ViewBag.output = output;
            //ViewBag.error = error;

            return View();
        }   
        private bool ProjectExists(int id) => _projectRepository.GetById(id) != null;
    }
}
