﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using NPC0100Project.Areas.Identity.Data;
using NPC0100Project.Utilities;

namespace NPC0100Project.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ForgotPasswordModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public ForgotPasswordModel(UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _userManager = userManager;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        [TempData]
        public string StatusMessage { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(Input.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    //return RedirectToPage("./ForgotPasswordConfirmation");
                    StatusMessage = "The user does not exist or is not confirmed.";
                    return RedirectToPage();
                }

                // For more information on how to enable account confirmation and password reset please 
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                var callbackUrl = Url.Page(
                    "/Account/ResetPassword",
                    pageHandler: null,
                    values: new { area = "Identity", code },
                    protocol: Request.Scheme);

                await _emailSender.SendEmailAsync(
                    Input.Email,
                    "Reset Password",
                    $@"<!DOCTYPE html>
                    <html>
                    <head>
                        <meta charset = "" utf - 8"" />
                        <title >#Subject#</title>
                    </head>
                    <body >
                        <div class=""card"" style=""width: 50%; margin-left: 20%;"">
                            <img src = ""https://www.litmus.com/wp-content/uploads/2020/04/5-of-the-best-password-reset-emails.png"" class=""card-img-top"" alt=""..."" style=""width: 70%;"">
                            <div class=""card-body"">
                              <h5 class=""card-title"">Reset your password</h5>
                              <p class=""card-text"">Someone has requested a link to change your passward at Projects.
                                  You can do this through the link below.
                              </p>
                              <a href = ""{HtmlEncoder.Default.Encode(callbackUrl)}"" class=""btn btn-primary"">Change my Password</a>
                            </ div >
                          </ div >
                    </body>
                    </html>"
                    //$"Please reset your password by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>."
                    );

                return RedirectToPage("./ForgotPasswordConfirmation");
            }

            return Page();
        }
    }
}
