﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NPC0100Project.Areas.Identity.Data;
using NPC0100Project.Authorization;
using NPC0100Project.Data;
using NPC0100Project.Utilities;

[assembly: HostingStartup(typeof(NPC0100Project.Areas.Identity.IdentityHostingStartup))]
namespace NPC0100Project.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {

                

                services.AddDbContext<AuthDbContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("AuthDbContextConnection")));



                services.AddDefaultIdentity<ApplicationUser>(options =>
                {

                    options.SignIn.RequireConfirmedAccount = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    //options.SignIn.RequireConfirmedAccount = true;
                })
                    .AddRoles<IdentityRole>()
                    .AddEntityFrameworkStores<AuthDbContext>();
                    //.AddPasswordValidator<UsernameAsPasswordValidator<ApplicationUser>>();

                // Authorization handlers.
                services.AddScoped<IAuthorizationHandler,
                                  ContactIsOwnerAuthorizationHandler>();

                services.AddSingleton<IAuthorizationHandler,
                                      ContactAdministratorsAuthorizationHandler>();

                //services.AddSingleton<IAuthorizationHandler,
                //                      ContactManagerAuthorizationHandler>();

            });
        }
    }
}