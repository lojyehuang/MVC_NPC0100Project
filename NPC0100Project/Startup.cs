using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NPC0100Project.Authorization;
using NPC0100Project.Data;
using NPC0100Project.Models;
using NPC0100Project.Repository;
using NPC0100Project.Utilities;

namespace NPC0100Project
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var emailConfig = Configuration
                .GetSection("EmailConfiguration")
                .Get<EmailConfiguration>();
                //.GetSection("SMTPConfig")
                //.Get<SMTPConfigModel>();
            services.AddSingleton(emailConfig);
            //從組態讀取登入逾時設定
            double LoginExpireMinute = this.Configuration.GetValue<double>("LoginExpireMinute");
            //註冊 CookieAuthentication，Scheme必填
            //services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(option =>
            //{
            //    //或許要從組態檔讀取，自己斟酌決定
            //    option.LoginPath = new PathString("/Home/Login");//登入頁
            //    option.LogoutPath = new PathString("/Home/Logout");//登出Action
            //    //用戶頁面停留太久，登入逾期，或Controller的Action裡用戶登入時，也可以設定↓
            //    option.ExpireTimeSpan = TimeSpan.FromMinutes(LoginExpireMinute);//沒給預設14天
            //    //↓資安建議false，白箱弱掃軟體會要求cookie不能延展效期，這時設false變成絕對逾期時間
            //    //↓如果你的客戶反應明明一直在使用系統卻容易被自動登出的話，你再設為true(然後弱掃policy請客戶略過此項檢查) 
            //    option.SlidingExpiration = false;
            //});
            //services.AddControllersWithViews();
            services.AddControllersWithViews(options => {
                //↓和CSRF資安有關，這裡就加入全域驗證範圍Filter的話，待會Controller就不必再加上[AutoValidateAntiforgeryToken]屬性
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            });
            services.AddRazorPages();
            services.AddDbContext<ProjectContext>(options => options.UseSqlServer(
                Configuration.GetConnectionString("Default")));
            //services.AddScoped<IProjectRepository,ProjectRepository>();
            services.AddScoped<IRepository<Project,int>, ProjectRepository>();


            //// Authorization handlers.
            //services.AddScoped<IAuthorizationHandler,
            //                      ContactIsOwnerAuthorizationHandler>();

            //services.AddSingleton<IAuthorizationHandler,
            //                      ContactAdministratorsAuthorizationHandler>();

            ////services.AddSingleton<IAuthorizationHandler,
            ////                      ContactManagerAuthorizationHandler>();

            services.AddAuthorization(options =>
            {
                options.FallbackPolicy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
            });

            services.AddSingleton<IEmailSender, EmailSender>();

            //services.AddControllers(config =>
            //{
            //    // using Microsoft.AspNetCore.Mvc.Authorization;
            //    // using Microsoft.AspNetCore.Authorization;
            //    var policy = new AuthorizationPolicyBuilder()
            //                     .RequireAuthenticatedUser()
            //                     .Build();
            //    config.Filters.Add(new AuthorizeFilter(policy));
            //});


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection(); //這樣的話，Controller、Action不必再加上[RequireHttps]屬性
            app.UseStaticFiles();

            app.UseRouting();
            //留意寫Code順序，先執行驗證...
            app.UseAuthentication();
            app.UseAuthorization();//Controller、Action才能加上 [Authorize] 屬性
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
        //pattern: "{controller=Project}/{action=Index}/{id?}");
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
