﻿using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using MimeKit;
using MailKit.Net.Smtp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Proxy;

namespace NPC0100Project.Utilities
{
    public class EmailSender:IEmailSender
    {
        //private readonly SMTPConfigModel _emailConfiguration;
        private readonly EmailConfiguration _emailConfiguration;


        //public EmailSender(SMTPConfigModel emailConfiguration)
        public EmailSender(EmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
        }

        //public async Task SendEmailAsync(string email, string subject, string message)
        //{
        //    MimeMessage mmessage = new MimeMessage();

        //    MailboxAddress from = new MailboxAddress("Admin",
        //    "admin@example.com");
        //    mmessage.From.Add(from);

        //    MailboxAddress to = new MailboxAddress("User",
        //    "hz171168@npc.com.tw");
        //    mmessage.To.Add(to);

        //    mmessage.Subject = "This is email subject";

        //    BodyBuilder bodyBuilder = new BodyBuilder();
        //    bodyBuilder.HtmlBody = "<h1>Hello World!</h1>";
        //    bodyBuilder.TextBody = "Hello World!";
        //    mmessage.Body = bodyBuilder.ToMessageBody();
        //    SmtpClient client = new SmtpClient();
        //    client.ProxyClient = new HttpProxyClient("10.3.3.36", 80);
        //    client.Connect("smtp.mailtrap.io", 2525,true);
        //    client.Authenticate("f73ecbdb4b4ad8", "9aa4b3f2a85ef8");
        //    await client.SendAsync(mmessage);
        //    client.Disconnect(true);
        //    client.Dispose();


        //    //MailMessage mail = new MailMessage
        //    //{
        //    //    Subject = subject,
        //    //    Body = message,
        //    //    From = new MailAddress(_emailConfiguration.SenderAddress, _emailConfiguration.SenderDisplayName),
        //    //    IsBodyHtml = _emailConfiguration.IsBodyHTML
        //    //};


        //    //mail.To.Add(email);
        //    ////foreach (var toEmail in userEmailOptions.ToEmails)
        //    ////{
        //    ////    mail.To.Add(toEmail);
        //    ////}

        //    //NetworkCredential networkCredential = new NetworkCredential(_emailConfiguration.UserName, _emailConfiguration.Password);

        //    //SmtpClient smtpClient = new SmtpClient
        //    //{
        //    //    Host = _emailConfiguration.Host,
        //    //    Port = _emailConfiguration.Port,
        //    //    EnableSsl = _emailConfiguration.EnableSSL,
        //    //    UseDefaultCredentials = _emailConfiguration.UseDefaultCredentials,
        //    //    Credentials = networkCredential
        //    //};

        //    //mail.BodyEncoding = Encoding.Default;

        //    //await smtpClient.SendMailAsync(mail);
        //}

        public async Task SendEmailAsync(string email, string subject, string message)
        {



            try
            {
                var mimeMessage = new MimeMessage();

                mimeMessage.From.Add(new MailboxAddress(_emailConfiguration.From, _emailConfiguration.From));

                mimeMessage.To.Add(new MailboxAddress(email));

                mimeMessage.Subject = subject;

                mimeMessage.Body = new TextPart("html")
                {
                    Text = message
                };

                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.ProxyClient = new HttpProxyClient("10.3.3.36", 80);
                    await client.ConnectAsync(_emailConfiguration.SmtpServer, _emailConfiguration.Port, false);


                    // Note: only needed if the SMTP server requires authentication
                    await client.AuthenticateAsync(_emailConfiguration.From, _emailConfiguration.Password);

                    await client.SendAsync(mimeMessage);

                    await client.DisconnectAsync(true);
                }

            }
            catch (Exception ex)
            {
                // TODO: handle exception
                throw new InvalidOperationException(ex.Message);
            }
        }
    }
}
